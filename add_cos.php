<?php

include "includes/config.php";
include "includes/functions.php";

if (!session_start()) {
    session_start();
}

if (isset($_SESSION['cart_id'])){
    $cart = new Cart($_SESSION['cart_id']);
} else {
    $cart = new Cart();
    $cart->save();
    $_SESSION['cart_id'] = $cart->getId();
}
$cart->add($_GET['product_id'], $_GET['quantity']);

header("Location: list_cos.php");