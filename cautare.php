<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">

<?php
     if (!session_start()) {
        session_start();
     }

      include "includes/config.php";
      include "includes/functions.php";
      include "parts/header.php";
      include "parts/meniu.php";

      if (isset($_GET['page'])) {
          $pag = $_GET['page'];
      }
      else {$pag = 1;}
      $ItemsPerPage = 12;
      $start = ($pag-1)*$ItemsPerPage;

      $table = 'product';
      $likeFilter['full_name'] = $_POST['produs'];
      $sortBy = 'price - discount';
      $dir = 'ASC';
      $prod = new Product();
      $prodSearch = $prod->select(null, $likeFilter, $start, $ItemsPerPage, $sortBy, $dir);
      //$prod = dbSelect($table, null, $likeFilter, $start, $ItemsPerPage, $sortBy);
      listare($prodSearch, 6);

      $total = dbSelect($table, null, $likeFilter);
      $ItemsNr = count($total);
      $totalPages = ceil($ItemsNr/$ItemsPerPage);


      include "parts/pagination.php";
      include "parts/footer.php";

?>

</div>

</body>
</html