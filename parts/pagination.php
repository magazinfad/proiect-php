<div class="row justify-content-md-center">
    <div class="col-sm-13">
        <ul class="pagination"><?php
            for($i = 1; $i <= $totalPages; $i++){
                if($i == $pag) {?>
                    <li class="page-item active"><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                    <?php
                }else{?>
                    <li class="page-item"><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
                }
            }?>
        </ul>
    </div>
</div>