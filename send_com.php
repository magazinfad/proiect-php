<?php
include "includes/config.php";
include "includes/functions.php";

session_start();

$com = new Orders();
$cart = new Cart($_SESSION['cart_id']);

$com->firstname = $_POST['prenume'];
$com->lastname = $_POST['nume'];
if (($com->firstname == null)||($com->lastname == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->firstname = filter_var($com->firstname, FILTER_SANITIZE_STRING);
$com->lastname = filter_var($com->lastname, FILTER_SANITIZE_STRING);

$com->email = $_POST['email'];
if ($com->email == null) {
    header("Location: fin_comand.php?formError=Trebuie completat campul de adresa de email");
    die();
}
$com->email = filter_var($com->email, FILTER_SANITIZE_EMAIL);
if (filter_var($com->email, FILTER_VALIDATE_EMAIL) === false) {
    header("Location: fin_comand.php?formError=Nu este o adresa de email valida");
    die();
}
$com->status = $_POST['statut'];
$com->phone = $_POST['tel'];
$com->street = $_POST['strada'];
if (($com->phone == null)||($com->street == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->phone = filter_var($com->phone,FILTER_SANITIZE_STRING);
$com->street = filter_var($com->street, FILTER_SANITIZE_STRING);

$com->county = $_POST['judet'];
$com->city = $_POST['oras'];
if (($com->county == null)||($com->city == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->county = filter_var($com->county, FILTER_SANITIZE_STRING);
$com->city = filter_var($com->city, FILTER_SANITIZE_STRING);

$com->pay = $_POST['plata'];
$com->value = $cart->getTotal();
$com->year = date("Y");
$com->month = date("m");
$com->day = date("d");

$com->save();

$cart->address = $_POST['strada'];
$cart->phone = $_POST['tel'];
$cart->email = $_POST['email'];
$cart->payment = $_POST['plata'];

$user = new Users();
$user->SelectOne(['email' => $_POST['email']]);
$cart->user_id = $user->getID();
$cart->save();

//$cart->generateOrder();

foreach($cart->getCartItems() as $itemCart) {
   $orderItem = new Order_items();
   $orderItem->prod_id = $itemCart->product_id;
   $orderItem->quantity = $itemCart->quantity;
   $orderItem->price = $itemCart->getProduct()->getFinalPrice();
   $orderItem->category_id = $itemCart->getProduct()->category_id;
   $orderItem->order_id = $com->getID();
   $orderItem->save();
   }
$to = "$com->email";
$headers = "From: sales@pcnet.ro";
$msg = "Comanda dvs. a fost inregistrata cu succes.";

mail($to,"Comanda dvs. la PC NET", $msg);

header("Location: list_cos.php");

