<?php


class Cart extends BaseEntity
{
public $user_id;
public $address;
public $phone;
public $email;
public $payment;

public function getUser() {
 return new Users($this->user_id);
}

public function getCartItems() {
    $data = dbSelect('cart_item',['cart_id'=>$this->getID()]);
    $result = [];
    foreach ($data as $line) {
        $result[] = new CartItem($line['id']);
    }
    return $result;
}

public function getTotal() {
    $total = 0;
    foreach ($this->getCartItems() as $cartItem){
        $total += $cartItem->getTotal();
    }
    return $total;
}

public function add($product_id, $quantity) {
    foreach ($this->getCartItems() as $cartItem){
        if ($cartItem->product_id == $product_id) {
           $cartItem->quantity += $quantity;
           echo $cartItem->quantity;echo "<br>";
           if ($cartItem->quantity <= 0) {
               $cartItem->delete();
               return;
           } else {
               var_dump($cartItem);
               $cartItem->save();
               return;
           }
       }
    }
    $cartItem = new CartItem();
    $cartItem->cart_id = $this->getID();
    $cartItem->product_id = $product_id;
    $cartItem->quantity = $quantity;
    $cartItem->save();
}

public function update($product_id, $quantity) {
   foreach ($this->getCartItems() as $cartItem){
       if ($cartItem->product_id == $product_id){
           $cartItem->quantity = $quantity;
           if ($cartItem <= 0){
               $cartItem->delete();
           } else {
               $cartItem->save();
           }
       }
   }
}

public function emptyCart(){
    foreach ($this->getCartItems() as $cartItem){
        $cartItem->delete();
    }
}

}