<?php
include "includes/config.php";
include "includes/functions.php";

$selData['email'] = $_POST['email'];
$selData['password'] = $_POST['parola'];

if (($selData['email'] == null )||($selData['password'] == null)) {
  header('Location: cont.php?logInError=Trebuie completate toate,  campurile!');
}
else {
    if (filter_var($selData['email'], FILTER_VALIDATE_EMAIL) === false) {
        header("Location: cont.php?logInError=Nu este o adresa de email valida");
        die();
    }
    $selData['password'] = filter_var($selData['password'], FILTER_SANITIZE_STRING);
    $selData['email'] = filter_var($selData['email'], FILTER_SANITIZE_EMAIL);
    $user = new Users;
    $user->selectOne(['email' => $selData['email']]);
    //$user = dbSelect($table, $selData);
    if ($user->email != null) {
        if (password_verify($selData['password'], $user->password)) {
            session_start();
            $_SESSION['user'] = $user->firstname;
            $_SESSION['userId'] = $user->getID();
            $_SESSION['userName'] = $user->lastname;
            $_SESSION['email'] = $user->email;
            $_SESSION['phone'] = $user->phone;

            header("Location: index.php");
        }
        else { header('Location: cont.php?logInError=Parola este gresita'); }
    } else {
        header('Location: cont.php?logInError=Email gresit!');
    }
}
?>