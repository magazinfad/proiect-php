<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
        <div class="container-fluid" style="width:85%">
            <?php
            session_start();
            include "includes/config.php";
            include "includes/functions.php";
            include "parts/header.php";
            include "parts/meniu.php";
            $product = new Product();
            $product->selectOne(['id' => $_GET['id']]);
            //$product = dbSelect('products',['id' => $_GET['id']]);
            if(isset($_GET['id'])){
                $_SESSION['id'] = $_GET['id'];
            }
            $produsList = new Product();
            $produsList->selectOne(['id' =>  $_SESSION['id']]);
            $produs = $produsList;
            $prodSim = new Product();
            $produseSimilare = $prodSim->select(['category_id' => $produs->category_id], null, 0, 6, null, null);
            
            //$produseSimilare = dbSelect('products', ['category_id' => $produs['category_id']],0,6);
            ?> <br />
            <div class="row">
                <div class="col-sm-2">
                   <img src="images/<?php echo $product->image; ?>" width="120">
                </div>
                <div class="col-sm-9 my-auto">
                    <h5><b>Adauga review pentru: </b><?php echo $product->name; ?></h5>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-8">
                    <h5><b>Rating: </b></h5>
                    <form method="post">
                        <div class="rate">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="text">1 star</label>
                        </div><br />
                        <br /><div class="form-group">
                            <h5><b>Review</b></h5>
                            <textarea class="form-control" rows="3" placeholder="Descrie experienta ta cu produsul" name="reviewMessage"></textarea>
                        </div>
                        <br />
                        <div class="form-group">
                            <h5><b>Titlu review </b>(optional)</h5>
                            <input type="text" class="form-control" placeholder="Foloseste o sugestie" name="reviewTitle">
                        </div><br />
                        <br /><button type="submit" class="btn btn-primary" name="reviewSubmit">Adauga review</button>&nbsp;&nbsp;
                        <a href="">Anuleaza</a>
                    </form>
                    <hr>
                    <?php
                    $revMes = new Review();
                    if(isset($_SESSION['user'])){
                        $username = $_SESSION['user'];
                    }else{
                        $username = 'Anonim';
                    }
                    if(isset($_POST['reviewSubmit'])){
                        if(isset($_POST['rate'])){
                            if($_POST['reviewMessage'] != '') {
                                $revMes->username = $username;
                                $revMes->product_id = $_GET['id'];
                                $revMes->rating = $_POST['rate'];
                                $revMes->message = $_POST['reviewMessage'];
                                $revMes->title = $_POST['reviewTitle'];
                                $revMes->add_date = date("Y-m-d");
                                $revMes->save();
                                //dbInsert('review', ['username' => $username, 'product_id' => $_GET['id'], 'rating' => $_POST['rate'], 'message' => $_POST['reviewMessage'], 'title' => $_POST['reviewTitle']]);
                            }else{
                                ?><script>
                                alert('Adauga un review!');
                                </script<?php
                            }
                        }else{
                            ?><script>
                                alert('Adauga un rating!');
                            </script<?php
                        }
                    }
                    ?>
                </div>
                <div class="col-sm-4">
                    <p>Probleme cu produsul sau transportul?</p>
                    <p>Adauga o sesizare <a href=""><b>aici</b></a></p>
                </div>
            </div>
            <h4>Produse similare</h4><br>
            <div class="row" >
                <?php

                $prodSim = new Product($_GET['id']);
                $simFilter['category_id'] = $prodSim->category_id;
                $produseSimilare = $prodSim->select($simFilter, null, 0, 6, null, null);
                foreach($produseSimilare as $produsSimilar){
                    if($produsSimilar->getID() != $_SESSION['id']) {
                        display($produsSimilar, $produsSimilar->getID());
                    }
                }
                ?>
           </div> <br />
           <?php include "parts/footer.php"; ?>
        </div>
    </body>
</html>

