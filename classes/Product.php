<?php

class Product extends BaseEntity
{

public $name;
public $full_name;
public $image;
public $description;
public $category_id;
public $price;
public $discount;
public $views;

public function getFinalPrice() {
    return $this->price - $this->discount;
}

public function getCategories() {
    $cat = $this->category_id;
    $data = dbSelect('category',['parent_id'=>$cat]);
    $result = [];
    foreach ($data as $line) {
    $result[] = new Category($line['id']);
    }
    return $result;
}

public function getImages() {
    $id = $this->id;
    $data = dbSelect('product_images',['product_id'=>$id]);
    $result = [];
    foreach ($data as $line) {
        $result[] = new Image($line['id']);
    }
    return $result;
}

public function views() {
    $id = $this->id;
    $data = dbSelectOne('product', ['id' => $id]);
    if ($data['views'] == null) {
        $data['views'] = 0;
    }
    $view = $data['views'] + 1;
    $this->views = $view;
    $this->save();
}

}