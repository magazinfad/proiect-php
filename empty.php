<?php

include "includes/config.php";
include "includes/functions.php";

session_start();
$cart = new Cart($_SESSION['cart_id']);
foreach ($cart->getCartItems() as $cartItem) {
    $cartItem->delete();
}
header("Location: index.php");